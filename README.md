# platformer-tutorial

based on [arcade](https://arcade.academy/examples/platform_tutorial/index.html)
tutorial.


## setup env

See [instructions](https://arcade.academy/installation_mac.html)

```
conda create -n arcade python=3.7
conda activate arcade
pip install PyObjC arcade
```
